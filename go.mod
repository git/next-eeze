module notabug.org/apiote/next-eeze

go 1.15

require (
	git.sr.ht/~sircmpwn/getopt v1.0.0
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
	github.com/keys-pub/go-libfido2 v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
