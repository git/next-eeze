package password

type NextPassword struct {
	Id           string
	Label        string
	Username     string
	Password     string
	Url          string
	Notes        string
	CustomFields string
	Status       int
	StatusCode   string
	Hash         string
	Folder       string
	Revision     string
	Share        *string
	Shared       bool
	CseType      string
	CseKey       string
	SseType      string
	Client       string
	Hidden       bool
	Trashed      bool
	Favorite     bool
	Editable     bool
	Edited       int
	Created      int
	Updated      int
}

func (n NextPassword) ToBarePassword() BarePassword {
	converted := BarePassword{
		Label:    n.Label,
		Username: n.Username,
		Password: n.Password,
		Url:      n.Url,
		Notes:    n.Notes,
	}
	return converted
}
