package password

type BarePassword struct {
	Label    string
	Username string
	Password string
	Url      string
	Notes    string
	//CustomFields []CustomField
	//Folder string
}
